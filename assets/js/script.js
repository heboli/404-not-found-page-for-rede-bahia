let bars = document.getElementsByClassName('bar');
let h1 = document.getElementsByTagName('H1');
h1 = h1[0];

firstColors = ["FFFFFF","DBDB15","00C0C0","00C000","C028C0","C00000","0000C0"]; 

for (var i = 0; i < 7; i++) {
		bars[i].style.backgroundColor = "#"+firstColors[i];
}

function changeBarColors(){
	for (var i = 7; i < bars.length; i+=2) {
		a = Math.round(Math.random() * 255);
		b = Math.round(Math.random() * 255);
		c = Math.round(Math.random() * 255);
		
		bars[i].style.backgroundColor = "rgb("+a+","+b+","+c+")";
	}

}
changeBarColors();
setInterval(changeBarColors, 2000);